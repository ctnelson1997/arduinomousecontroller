const int LEFT_CONTROL_APIN = 5;
const int RIGHT_CONTROL_APIN = 4;
const int CLICK_DPIN = 2;

const int BAUD_COMM_CHANNEL = 9600;

const int READ_DELAY_MS = 10;

String readValsAsJSON();

void setup() {
  Serial.begin(BAUD_COMM_CHANNEL);
  
  pinMode(CLICK_DPIN, INPUT);
}

void loop() {
  String values = readValsAsJSON();
  Serial.println(values);
  delay(READ_DELAY_MS);
}

String readValsAsJSON() {

  int leftVal = analogRead(LEFT_CONTROL_APIN);
  int rightVal = analogRead(RIGHT_CONTROL_APIN);
  int clickVal = digitalRead(CLICK_DPIN);
  
  String json = "{ ";

  json += String("\"left_val\":") + String(leftVal) + String(", ");
  json += String("\"right_val\":") + String(rightVal) + String(", ");
  json += String("\"click_val\":") + String(clickVal) + String(" ");
  
  json += String('}');

  return json;
}

