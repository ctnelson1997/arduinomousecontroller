package edu.uwplatt.nelsoncole.amc.entities;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ArduinoMouseReading
{
   
   private static final String KEY_LEFT_VAL = "left_val";
   private static final String KEY_RIGHT_VAL = "right_val";
   private static final String KEY_CLICK_VAL = "click_val";
   
   private long leftVal;
   private long rightVal;
   private boolean clicked;
   
   private ArduinoMouseReading(long leftVal, long rightVal, boolean clicked) {
      this.leftVal = leftVal;
      this.rightVal = rightVal;
      this.clicked = clicked;
   }
   
   public long getLeftVal() {
      return this.leftVal;
   }
   
   public long getRightVal() {
      return this.rightVal;
   }
   
   public boolean isClicked() {
      return this.clicked;
   }
   
   public static ArduinoMouseReading constructFromJSON(String json) {
      try {
         JSONObject jsonObj = (JSONObject) new JSONParser().parse(json);
         long leftVal = (long) jsonObj.get(KEY_LEFT_VAL);
         long rightVal = (long) jsonObj.get(KEY_RIGHT_VAL);
         boolean clickVal = ((long) jsonObj.get(KEY_CLICK_VAL)) != 0;
         
         return ArduinoMouseReading.constructFromValues(leftVal, rightVal, clickVal);
      } catch (Exception e) {
         e.printStackTrace();
      }
      return ArduinoMouseReading.constructFromValues(0, 0, false);
   }
   
   public static ArduinoMouseReading constructFromValues(long leftVal, long rightVal, boolean clicked) {
      return new ArduinoMouseReading(leftVal, rightVal, clicked);
   }
}
