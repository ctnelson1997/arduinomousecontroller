package edu.uwplatt.nelsoncole.amc.entities;

import java.awt.Dimension;
import java.awt.Toolkit;

import edu.uwplatt.nelsoncole.amc.helpers.MouseMover;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Cole Nelson
 */
public class ScreenPosition
{
   private static final int DEFAULT_SCREEN_RES_X = 1920;
   private static final int DEFAULT_SCREEN_RES_Y = 1080;
   
   private static final int ARDUINO_MAX_ADC = 1024;
   
   private static int screenResX = DEFAULT_SCREEN_RES_X;
   private static int screenResY = DEFAULT_SCREEN_RES_Y;
   
   private int x;
   private int y;
   private boolean click;
   
   private ScreenPosition(int x, int y, boolean click) {
      this.x = x;
      this.y = y;
      this.click = click;
   }
   
   public static void init() {
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      ScreenPosition.setScreenResolution((int) screenSize.getWidth(), (int) screenSize.getHeight());
   }  
   
   public int getX() {
      return this.x;
   }
   
   public int getY() {
      return this.y;
   }
   
   public boolean isClick() {
      return this.click;
   }
   
   public void moveCursorToPosition() {
      MouseMover.move(this);
   }
   
   public static void setScreenResolution(int screenResX, int screenResY) {
      ScreenPosition.screenResX = screenResX;
      ScreenPosition.screenResY = screenResY;
   }
   
   public static ScreenPosition constructFromJSON(String json) {
      return ScreenPosition.constructFromArduinoMouseReading(ArduinoMouseReading.constructFromJSON(json));
   }
   
   public static ScreenPosition constructFromArduinoMouseReading(ArduinoMouseReading amr) {
      return new ScreenPosition(
              (int) (amr.getLeftVal() * (screenResX / (float) ARDUINO_MAX_ADC)),
              (int) (amr.getRightVal() * (screenResY / (float) ARDUINO_MAX_ADC)),
              amr.isClicked()
      );
   }
   
   public static ScreenPosition constructFromValues(int x, int y, boolean click) {
      return new ScreenPosition(x, y, click);
   }
}
