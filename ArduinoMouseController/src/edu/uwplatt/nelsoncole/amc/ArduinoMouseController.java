package edu.uwplatt.nelsoncole.amc;

import edu.uwplatt.nelsoncole.amc.helpers.ArduinoHandler;
import edu.uwplatt.nelsoncole.amc.helpers.MouseHandler;

public class ArduinoMouseController {

   public static void main(String[] args) {
      ArduinoHandler arduinoHandler = new ArduinoHandler();
      arduinoHandler.init();
      MouseHandler.init(arduinoHandler);
   }

}
