package edu.uwplatt.nelsoncole.amc.helpers;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ArduinoHandler implements SerialPortEventListener {
   
   private static volatile String lastJSON = new String();
   
   private SerialPort serialPort;
   private BufferedReader input;

   private static final int TIME_OUT = 5000;
   private static final int BAUD_RATING = 9600;
   private static final String COM_PORT = "COM4";

   public void init() {
      try {
         serialPort = (SerialPort) CommPortIdentifier.getPortIdentifier(COM_PORT).open("AMC", TIME_OUT);
         serialPort.setSerialPortParams(BAUD_RATING, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
         input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));

         serialPort.addEventListener(this);
         serialPort.notifyOnDataAvailable(true);
      } catch (Exception e) {
         e.printStackTrace();
         System.exit(-1);
      }
   }
   
   public String getLastJSON() {
      return lastJSON;
   }

   public synchronized void close() {
      if (serialPort != null) {
         serialPort.removeEventListener();
         serialPort.close();
      }
   }

   public synchronized void serialEvent(SerialPortEvent oEvent) {
      if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
         try {
            ArduinoHandler.lastJSON = input.readLine();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
   }
}
