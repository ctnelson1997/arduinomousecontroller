package edu.uwplatt.nelsoncole.amc.helpers;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.uwplatt.nelsoncole.amc.entities.ScreenPosition;

/**
 *
 * @author Cole Nelson
 */
public class MouseHandler
{
   private static final int PRIMING_DELAY = 1000;
   private static final int READ_DELAY = 50;
   
   public static void init(ArduinoHandler arduinoHandler) {
      try {
         Thread.sleep(PRIMING_DELAY);
         while(true) {
            Thread.sleep(READ_DELAY);
            MouseMover.move(ScreenPosition.constructFromJSON(arduinoHandler.getLastJSON()));
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}
