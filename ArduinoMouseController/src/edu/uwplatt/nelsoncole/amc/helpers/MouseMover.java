package edu.uwplatt.nelsoncole.amc.helpers;

import java.awt.Robot;
import java.awt.event.InputEvent;

import edu.uwplatt.nelsoncole.amc.entities.ScreenPosition;

public class MouseMover
{
   private static Robot robot = null;
   
   static {
      try {
         robot = new Robot();
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
   
   public static void move(ScreenPosition pos) {
      robot.mouseMove(0, 0);
      robot.mouseMove(pos.getX(), pos.getY());
      if(pos.isClick()) {
         robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
         robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
      }
   }
}
